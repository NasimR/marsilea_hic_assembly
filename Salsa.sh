#!/bin/bash
#SBATCH --job-name=Salsa
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 2
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH --mem=10G
#SBATCH -o salsa_%j.out
#SBATCH -e salsa_%j.err

module load python/2.7.14 
module load boost/1.65.1
module load networkx/2.1
python /home/CAM/nrahmatpour/Nasim/Marsilea_genome/V4_Hi_C/whole_bam/5_iteration/SALSA/run_pipeline.py -a Marsilea_vestita_genome_v1.fasta -l Marsilea_vestita_genome_v1.fasta.fai -b alignment.bed -e GATC -o scaffolds -i 9 -m yes 
