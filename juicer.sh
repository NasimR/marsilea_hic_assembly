#!/bin/bash
#SBATCH --job-name=juicer
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH -p general
#SBATCH -q general
#SBATCH --mem=100G
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o juicer_%j.out
#SBATCH -e juicer_%j.err

module load java-sdk/1.8.0_92
module load bwa/0.7.17
#awk 'BEGIN{OFS="\t"}{print $1, $NF}' Marsilea_v3_restrction_Sau3AI.txt > Marsilea_v3.chrom.sizes
-Djava.io.tmpdir=/home/CAM/nrahmatpour/Nasim/Marsilea_genome/V3_Hi_C/filtered_Bam/organelle_removed/25_organelle_removed/juicer
bash /labs/Wegrzyn/local_software/juicer/SLURM/scripts/juicer.sh -g Mvestita_v1 -y /home/CAM/nrahmatpour/Nasim/Marsilea_genome/V3_Hi_C/filtered_Bam/organelle_removed/25_organelle_removed/juicer/Marsilea_v3_restrction_Sau3AI.txt -z /home/CAM/nrahmatpour/Nasim/Marsilea_genome/V3_Hi_C/filtered_Bam/organelle_removed/25_organelle_removed/juicer/Marsilea_vestita_genome_v1.fasta -p /home/CAM/nrahmatpour/Nasim/Marsilea_genome/V3_Hi_C/filtered_Bam/organelle_removed/25_organelle_removed/juicer/Marsilea_v3.chrom.sizes -q short -Q 2:00:00 -l medium -L 12:00:00 -t 8


