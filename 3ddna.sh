#!/bin/bash
#SBATCH --job-name=3ddna
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH -o 3ddna_%j.out
#SBATCH -e 3ddna_%j.err

hostname
echo "\nStart time:"
date

bash /labs/Wegrzyn/local_software/3d-dna/run-asm-pipeline.sh -i 10000 --editor-coarse-resolution 250000 --editor-coarse-region 500000 --editor-repeat-coverage 20 /home/CAM/nrahmatpour/Nasim/Marsilea_genome/V3_Hi_C/filtered_Bam/organelle_removed/25_organelle_removed/3ddna/Marsilea_vestita_genome_v1.fasta /home/CAM/nrahmatpour/Nasim/Marsilea_genome/V3_Hi_C/filtered_Bam/organelle_removed/25_organelle_removed/3ddna/merged_nodups.txt
echo "\nEnd time:"
date

