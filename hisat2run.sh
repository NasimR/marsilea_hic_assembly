#!/bin/bash
#SBATCH --job-name=hisat2run
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=nasim.rahmatpour@uconn.edu
#SBATCH --mem=50G
#SBATCH -o hisat2run_%j.out
#SBATCH -e hisat2run_%j.err
module load hisat2/2.2.1
module load samtools
hisat2 -x  Mvestita_masked -1 12_2_paired_1.fq -2 12_2_paired_2.fq --dta -p 16 -S 12_2_paired.sam
samtools view -@ 16 -uhS 12_2_paired.sam | samtools sort -@ 16 -o 12_2_sorted_paired.bam

hisat2 -x Mvestita_masked -1 1-2B_paired_1.fq -2 1-2B_paired_2.fq --dta -p 16 -S 1-2B_paired.sam
samtools view -@ 16 -uhS 1-2B_paired.sam | samtools sort -@ 16 -o 1-2B_sorted_paired.bam

hisat2 -x Mvestita_masked -1 1-2hr_paired_1.fq -2 1-2hr_paired_2.fq --dta -p 16 -S 1-2hr_paired.sam
samtools view -@ 16 -uhS 1-2hr_paired.sam | samtools sort -@ 16 -o 1-2hr_sorted_paired.bam

hisat2 -x Mvestita_masked -1 35_4_paired_1.fq -2 35_4_paired_2.fq --dta -p 16 -S 35_4_paired.sam
samtools view -@ 16 -uhS 35_4_paired.sam | samtools sort -@ 16 -o 35_4_sorted_paired.bam

hisat2 -x Mvestita_masked -1 3-5B_paired_1.fq -2 3-5B_paired_2.fq --dta -p 16 -S 3-5B_paired.sam
samtools view -@ 16 -uhS 3-5B_paired.sam | samtools sort -@ 16 -o 3-5B_sorted_paired.bam

hisat2 -x Mvestita_masked -1 3-5hr_paired_1.fq -2 3-5hr_paired_2.fq --dta -p 16 -S 3-5hr_paired.sam
samtools view -@ 16 -uhS 3-5hr_paired.sam | samtools sort -@ 16 -o 3-5hr_sorted_paired.bam

hisat2 -x Mvestita_masked -1 68_5_paired_1.fq -2 68_5_paired_2.fq --dta -p 16 -S 68_5_paired.sam
samtools view -@ 16 -uhS 68_5_paired.sam | samtools sort -@ 16 -o 68_5_sorted_paired.bam

hisat2 -x Mvestita_masked -1 6-8B_paired_1.fq -2 6-8B_paired_2.fq --dta -p 16 -S 6-8B_paired.sam
samtools view -@ 16 -uhS 6-8B_paired.sam | samtools sort -@ 16 -o 6-8B_sorted_paired.bam

hisat2 -x Mvestita_masked -1 6-8hr_paired_1.fq -2 6-8hr_paired_2.fq --dta -p 16 -S 6-8hr_paired.sam
samtools view -@ 16 -uhS 6-8hr_paired.sam | samtools sort -@ 16 -o 6-8hr_sorted_paired.bam

hisat2 -x Mvestita_masked -1 Actinomycin_paired_1.fq -2 Actinomycin_paired_2.fq --dta -p 16 -S Actinomycin_paired.sam
samtools view -@ 16 -uhS Actinomycin_paired.sam | samtools sort -@ 16 -o Actinomycin_sorted_paired.bam

hisat2 -x Mvestita_masked -1 AlphaAma_paired_1.fq -2 AlphaAma_paired_2.fq --dta -p 16 -S AlphaAma_paired.sam
samtools view -@ 16 -uhS AlphaAma_paired.sam | samtools sort -@ 16 -o AlphaAma_sorted_paired.bam

hisat2 -x Mvestita_masked -1 CPSF_paired_1.fq -2 CPSF_paired_2.fq --dta -p 16 -S CPSF_paired.sam
samtools view -@ 16 -uhS CPSF_paired.sam | samtools sort -@ 16 -o CPSF_sorted_paired.bam

hisat2 -x Mvestita_masked -1 MvU620range1_paired_1.fq -2 MvU620range1_paired_2.fq --dta -p 16 -S MvU620range1_paired.sam
samtools view -@ 16 -uhS MvU620range1_paired.sam | samtools sort -@ 16 -o MvU620range1_sorted_paired.bam

hisat2 -x Mvestita_masked -1 MvU620range2_paired_2.fq -2 MvU620range2_paired_2.fq --dta -p 16 -S MvU620range2_paired.sam
samtools view -@ 16 -uhS MvU620range2_paired.sam | samtools sort -@ 16 -o MvU620range2_sorted_paired.bam

hisat2 -x Mvestita_masked -1 PRP_paired_1.fq -2 PRP_paired_2.fq --dta -p 16 -S PRP_paired.sam
samtools view -@ 16 -uhS PRP_paired.sam | samtools sort -@ 16 -o PRP_sorted_paired.bam

hisat2 -x Mvestita_masked -1 RdRp_paired_1.fq -2 RdRp_paired_2.fq --dta -p 16 -S RdRp_paired.sam
samtools view -@ 16 -uhS RdRp_paired.sam | samtools sort -@ 16 -o RdRp_sorted_paired.bam

hisat2 -x Mvestita_masked -1 RNApolIII_paired_1.fq -2 RNApolIII_paired_2.fq --dta -p 16 -S RNApolIII_paired.sam
samtools view -@ 16 -uhS RNApolIII_paired.sam | samtools sort -@ 16 -o RNApolIII_sorted_paired.bam

hisat2 -x Mvestita_masked -1 SPD_paired_1.fq -2 SPD_paired_2.fq --dta -p 16 -S SPD_paired.sam
samtools view -@ 16 -uhS SPD_paired.sam | samtools sort -@ 16 -o SPD_sorted_paired.bam

hisat2 -x Mvestita_masked -1 leaf_paired_1.fq -2 leaf_paired_2.fq --dta -p 16 -S leaf_paired.sam
samtools view -@ 16 -uhS leaf_paired.sam | samtools sort -@ 16 -o leaf_sorted_paired.bam

